﻿using System.Collections;
using GoogleTranslator;
using System.Xml.Linq;
using Collection = System.Collections.Generic.Dictionary<int, Facade.Word>;
using System.Text.RegularExpressions;

namespace Facade
{
    public class FacadeFileBase
    {
        private CollectionWord collectionWord = new();
        private Translator translator = new();
        private Tuple<string, string> languages = new("", "");

        public CollectionWord Collection => collectionWord;

        public string GetOriginalLang => languages.Item1;
        public string GetTranslateLang => languages.Item2;

        private static FacadeFileBase? instance;
        private readonly string path;

        public string this[int indexator]
        {
            get
            {
                return $"{GetOriginalLang}: {collectionWord[indexator].EnteredWord}\n" +
                       $"{GetTranslateLang}: {collectionWord[indexator].Translate}";
            }
        }

        private FacadeFileBase(string path)
        {
            this.path = path;
        }

        public static FacadeFileBase Instance(string path)
        {
            return instance ??= new FacadeFileBase(path); 
        }

        public void SetLanguage(Language from, Language to)
        {
            if (from == to)
                throw new ArgumentException("The languages is are the same, please choose another one!"); 
           languages = new Tuple<string, string>(from.ToString(), to.ToString());
        }

        public void AddNewWord(string EnteredWord)
        {
            collectionWord.Add(EnteredWord, Path.IsRefNull(translator.Translate(EnteredWord, GetOriginalLang, GetTranslateLang)));
        }

        public static void DeleteXML(int id)
        {
            if (!FileWork.Delete(id))
                throw new IndexOutOfRangeException();
        }
        public void Save()
        {
            collectionWord.Add(path, languages);
        }
    }

    public class Word
    {
        public string EnteredWord { get; private set; }
        public string Translate { get; private set; }

        public Word(string EnteredWord, string Translate)
        {
            (this.EnteredWord, this.Translate) = (EnteredWord, Translate);
        }
        
    }

    public class CollectionWord : IEnumerable<CollectionWord>, 
                                  ICloneable
    {
        private readonly Collection collectionWord = new();
        private int WordCount = 0;
        public int Count => WordCount;

        public CollectionWord() { }

        public void Add(string EnteredWord, string Translate)
        {
            collectionWord.Add(WordCount, new Word(EnteredWord, Translate));
            WordCount++;
        }

        public void Delete(int id)
        {
            collectionWord.Remove(id);
        }

        public void Clear()
        {
            collectionWord.Clear();
        }

        public Word this[int id]
        {
            get
            {
                if (0 > id || id > WordCount)
                    throw new IndexOutOfRangeException("Index bound of range");
                return collectionWord[id];
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public IEnumerator<CollectionWord> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    public static class FileWork 
    {
        private static int Counter = GetGreatestId + 1;

        public static CollectionWord LoadCollection()
        {
            return new CollectionWord();
        }

        public static int GetGreatestId
        {
            get
            {
                var i = XDocument
                        .Load(Path.GetPath)
                        .Element("CollectionWord")
                        ?.Elements("id")
                        ?.LastOrDefault()
                        ?.Attribute("id");

                if (int.TryParse(i?.Value, out int index) && i != null)
                    return index;
                else
                    return 1;

            }
        }

        public static void Add(this CollectionWord words, string path, Tuple<string, string> tuple)
        {
            var xDocument = XDocument.Load(Path.GetPath).Element("CollectionWord");

            for (int i = 0; i < words.Count; i++)
            {
                xDocument?.Add(TagRoot(words[i].EnteredWord, tuple.Item1, words[i].Translate, tuple.Item2));
                Counter++;
            }
            xDocument?.Save(path);
        }

        public static XElement TagRoot(string enteredWord, string atribureEnteredWord, string translate, string atriburetranslat)
        {
            return
                new XElement
                (
                    new XElement("id", new XAttribute("id", Counter),
                            TagText(atribureEnteredWord, enteredWord),
                            TagText(atriburetranslat, translate)
                                )

                );
        }

        public static bool Delete(int id)
        {
            var xDocument = XDocument.Load(Path.GetPath).Element("CollectionWord");
            var obj = xDocument?.Elements("id").FirstOrDefault(i => i?.Attribute("id")?.Value == id.ToString());
            if (xDocument == null)
                return false;
            obj?.Remove();
            xDocument?.Save(Path.GetPath);
            return true;
        }

        public static int GetIdByWord(string word)
        {
            return Convert.ToInt32(XDocument.Load(Path.GetPath).Element("CollectionWord")?.Elements("id")?.FirstOrDefault(i => i?.Element("Text")?.Value == word)?.Attribute("id")?.Value);
        }

        public static XElement TagText(string atribute, string value)
        {
            return new XElement("Text", new XAttribute("lang", atribute), value);
        }
    }

    public static class TranslateParser
    {
        public static CollectionWord Parser(string EnteredWord)
        {
            var xdoc = XDocument.Load(Path.GetPath).Element("CollectionWord");
            var i = xdoc?.Elements("id").FirstOrDefault(i => i?.Element("Text")?.Value == EnteredWord);

            return new CollectionWord
            {
                { EnteredWord, i.Elements("Text").FirstOrDefault(t => t?.Value != EnteredWord)?.Value }
            };
        }
    }

    public static class Path
    {
        public static string GetPath => Environment
                                       .CurrentDirectory
                                       .Replace("\\bin\\Debug\\net6.0", "\\Dictionary.xml");

        public static T IsRefNull<T>(T? value)
        {
            if (value == null)
                throw new NullReferenceException();
            return value;
        }
    }

    public enum Language
    {
        Afrikaans,
        Albanian,
        Arabic,
        Armenian,
        Azerbaijani,
        Basque,
        Belarusian,
        Bengali,
        Bulgarian,
        Catalan,
        Chinese,
        Croatian,
        Czech,
        Danish,
        Dutch,
        English,
        Esperanto,
        Estonian,
        Filipino,
        Finnish,
        French,
        Galician,
        German,
        Georgian,
        Greek,
        Haitian,
        Hebrew,
        Hindi,
        Hungarian,
        Icelandic,
        Indonesian,
        Irish,
        Italian,
        Japanese,
        Korean,
        Lao,
        Latin,
        Latvian,
        Lithuanian,
        Macedonian,
        Malay,
        Maltese,
        Norwegian,
        Persian,
        Polish,
        Portuguese,
        Romanian,
        Russian,
        Serbian,
        Slovak,
        Slovenian,
        Spanish,
        Swahili,
        Swedish,
        Tamil,
        Telugu,
        Thai,
        Turkish,
        Ukrainian,
        Urdu,
        Vietnamese,
        Welsh,
        Yiddish,
    }

    public static class Program
    {
        public static void Main()
        {

            FacadeFileBase facadeFileBase = FacadeFileBase.Instance(Path.GetPath);

            facadeFileBase.SetLanguage(Language.English, 
                                       Language.Ukrainian);

            facadeFileBase.AddNewWord("The language");
            facadeFileBase.AddNewWord("Hello");
            facadeFileBase.AddNewWord("Adolf Hitler");

            Console.WriteLine(facadeFileBase[0]);
           
        }
    }
}