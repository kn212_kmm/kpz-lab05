﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace GoogleTranslator
{
    public class Translator
    {
        #region Properties

        /// <summary>
        /// Gets the supported languages.
        /// </summary>
        public static IEnumerable<string> Languages
        {
            get
            {
                if (_languageModeMap == null)
                    throw new ArgumentNullException("_languageModeMap == null");
                return _languageModeMap.Keys.OrderBy(p => p);
            }
        }

        /// <summary>
        /// Gets the time taken to perform the translation.
        /// </summary>
        public TimeSpan TranslationTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the url used to speak the translation.
        /// </summary>
        /// <value>The url used to speak the translation.</value>
        public string? TranslationSpeechUrl
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the error.
        /// </summary>
        public Exception? Error
        {
            get;
            private set;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Translates the specified source text.
        /// </summary>
        /// <param name="sourceText">The source text.</param>
        /// <param name="sourceLanguage">The source language.</param>
        /// <param name="targetLanguage">The target language.</param>
        /// <returns>The translation.</returns>
        public string? Translate(string sourceText, string sourceLanguage, string targetLanguage)
        {
            Error = null;
            TranslationSpeechUrl = null;
            TranslationTime = TimeSpan.Zero;
            DateTime tmStart = DateTime.Now;
            string translation = "";

            try
            {
                string translationFromGoogle = "";

                string url = string.Format("https://translate.googleapis.com/translate_a/single?client=gtx&sl={0}&tl={1}&dt=t&q={2}",
                                            LanguageEnumToIdentifier(sourceLanguage),
                                            LanguageEnumToIdentifier(targetLanguage),
                                            HttpUtility.UrlEncode(sourceText));

#pragma warning disable SYSLIB0014 // Тип или член устарел
                using (WebClient wc = new())
                {
                    wc.Headers.Add("user-agent",
                                   "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
                    wc.Encoding = System.Text.Encoding.UTF8;
                    translationFromGoogle = wc.DownloadString(url);
                }
#pragma warning restore SYSLIB0014 //устарел

                var json = JsonConvert.DeserializeObject(translationFromGoogle);
                var firs_json = IsRefNull(json) as JArray;
                var second_json = IsRefNull(firs_json).First() as JArray;
                var third_json = IsRefNull(second_json).First() as JArray;
                var fourth_json = IsRefNull(third_json).First() as JValue;
                translation = IsRefNull(
                                IsRefNull(
                                    IsRefNull(
                                              fourth_json
                                             )
                                             .Value
                                          ).ToString()
                                       );

                string token = "{0}&tl={1}&total=1&idx=0&textlen={2}&client=gtx\"";
                string format = "https://translate.googleapis.com/translate_tts?ie=UTF-8&q=" + token;

                if (translation == null)
                    throw new ArgumentNullException();

                TranslationSpeechUrl = string.Format(format, HttpUtility.UrlEncode(translation), LanguageEnumToIdentifier(targetLanguage), translation.Length);
            }
            catch (Exception ex)
            {
                Error = ex;
            }

            // Return result
            TranslationTime = DateTime.Now - tmStart;
            return translation;
        }

        public static T IsRefNull<T>(T? value)
        {
            if (value == null)
                throw new NullReferenceException();
            return value;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Converts a language to its identifier.
        /// </summary>
        /// <param name="language">The language."</param>
        /// <returns>The identifier or <see cref="string.Empty"/> if none.</returns>
        private static string? LanguageEnumToIdentifier(string language)
        {
            string? mode = "";
            _languageModeMap.TryGetValue(language, out mode);
            return mode;
        }

        /// <summary>
        /// Ensures the translator has been initialized.
        /// </summary>
        private static readonly Dictionary<string, string> _languageModeMap = new()
        {
                    { "Afrikaans", "af" },
                    { "Albanian", "sq" },
                    { "Arabic", "ar" },
                    { "Armenian", "hy" },
                    { "Azerbaijani", "az" },
                    { "Basque", "eu" },
                    { "Belarusian", "be" },
                    { "Bengali", "bn" },
                    { "Bulgarian", "bg" },
                    { "Catalan", "ca" },
                    { "Chinese", "zh-CN" },
                    { "Croatian", "hr" },
                    { "Czech", "cs" },
                    { "Danish", "da" },
                    { "Dutch", "nl" },
                    { "English", "en" },
                    { "Esperanto", "eo" },
                    { "Estonian", "et" },
                    { "Filipino", "tl" },
                    { "Finnish", "fi" },
                    { "French", "fr" },
                    { "Galician", "gl" },
                    { "German", "de" },
                    { "Georgian", "ka" },
                    { "Greek", "el" },
                    { "Haitian Creole", "ht" },
                    { "Hebrew", "iw" },
                    { "Hindi", "hi" },
                    { "Hungarian", "hu" },
                    { "Icelandic", "is" },
                    { "Indonesian", "id" },
                    { "Irish", "ga" },
                    { "Italian", "it" },
                    { "Japanese", "ja" },
                    { "Korean", "ko" },
                    { "Lao", "lo" },
                    { "Latin", "la" },
                    { "Latvian", "lv" },
                    { "Lithuanian", "lt" },
                    { "Macedonian", "mk" },
                    { "Malay", "ms" },
                    { "Maltese", "mt" },
                    { "Norwegian", "no" },
                    { "Persian", "fa" },
                    { "Polish", "pl" },
                    { "Portuguese", "pt" },
                    { "Romanian", "ro" },
                    { "Russian", "ru" },
                    { "Serbian", "sr" },
                    { "Slovak", "sk" },
                    { "Slovenian", "sl" },
                    { "Spanish", "es" },
                    { "Swahili", "sw" },
                    { "Swedish", "sv" },
                    { "Tamil", "ta" },
                    { "Telugu", "te" },
                    { "Thai", "th" },
                    { "Turkish", "tr" },
                    { "Ukrainian", "uk" },
                    { "Urdu", "ur" },
                    { "Vietnamese", "vi" },
                    { "Welsh", "cy" },
                    { "Yiddish", "yi" }
                };

        #endregion
    }
}